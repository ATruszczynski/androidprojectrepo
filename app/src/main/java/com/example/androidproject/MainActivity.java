package com.example.androidproject;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidproject.FirebaseUtility.FirestoreDB;
import com.example.androidproject.FirebaseUtility.Friendship;
import com.example.androidproject.FirebaseUtility.Rating;
import com.example.androidproject.FirebaseUtility.Series;
import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.FirebaseUtility.User;
import com.example.androidproject.Fragments.FriendsFragment;
import com.example.androidproject.Fragments.LogoutFragment;
import com.example.androidproject.Fragments.SeriesFragment;
import com.example.androidproject.Fragments.YouFragment;
import com.example.androidproject.GeneralUtility.FavListAdapter;
import java.util.ArrayList;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.sql.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements FirestoreDB.UserListInterface, FirestoreDB.SeriesListInterface,
                                                                FirestoreDB.CompleteInterface, FirestoreDB.RatingsInterface
{
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;

    private static Fragment curr = null;
    public static User currUser;

    String myId = "0";

    //Button logOutBtn;
    FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener  authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//SHA1: C7:18:FD:78:BC:BC:A0:C6:6C:C5:3E:6A:99:45:59:4D:74:C3:1F:DA

        SetUpNavDrawer();
        FirestoreDB db = new FirestoreDB();
        //db.addSeries(new Series(-1, "Title", 2019));
        //SetUpDB();
        myId = firebaseAuth.getInstance().getUid();
        if(curr == null)
            showYouFragment();
        else
            showFragment(curr);



        db.addCompleteListener(this);

        firebaseAuth = FirebaseAuth.getInstance();





        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user == null){
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }
        };


    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Button logOutBtn = findViewById(R.id.logOutBtn);
        //final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });

    }

    public void SetUpDB()
    {
        FirestoreDB db = new FirestoreDB();

        List<User> users = new LinkedList<>();
        List<Rating> ratings = new LinkedList<>();
        final List<Series> series = new LinkedList<>();
        List<Friendship> friends = new LinkedList<>();

        /*users.add(new User("u0", "Fofok", "m", 19));
        users.add(new User("u1", "Kubuś", "m", 15));
        users.add(new User("u2", "Anna Maria", "f", 32));*/

        series.add(new Series("Raj na ziemi", "Hotel Paradise",
                                "Adela Winforth accidently finds herself in the interdimensional hotel, run by what seems to be godess with extremely peculiar sense of humour.",
                                8.97, 1200, 2020, 35, "Comedy", new ArrayList<String>(Arrays.asList("Poland")),
                                 new ArrayList<String>(Arrays.asList("Aleksander Truszczyński", "Randomy w internecie")), new ArrayList<String>(Arrays.asList("A.B.C. Zeitler", "Amestris Dudan"))));
        series.add(new Series("Raj na ziemi 2: Asy w rękawie", "Hotel Paradise 2: Aces in the hole",
                "After taking over Earth ER-6969 for shits and giggles, Adela Winforth embarks on mission to become the greates pirate king in the entire multiverse.",
                5.45, 2102, 2023, 15, "Comedy", new ArrayList<String>(Arrays.asList("USA")),
                new ArrayList<String>(Arrays.asList("David Benioff", "D.B. Weiss")), new ArrayList<String>(Arrays.asList("A.B.C. Zeitler", "Amestris Dudan"))));


        /*ratings.add(new Rating("u0",0, 10));
        ratings.add(new Rating("u0",1, 10));
        ratings.add(new Rating("u0",2, 2));
        ratings.add(new Rating("u1",0, 4));
        ratings.add(new Rating("u1",1, 2));
        ratings.add(new Rating("u2",3, 9));

        friends.add(new Friendship("1", "u0", "u1"));
        friends.add(new Friendship("2", "u0", "u2"));
        friends.add(new Friendship("2", "u1", "u2"));*/

        for(User u: users)
            db.addUser(u);

        db.addSeries(series);
        for(Rating r: ratings)
            db.addRating(r);
        for(Friendship f: friends)
            db.addFriendship(f);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    void SetUpNavDrawer()
    {
        dl = (DrawerLayout)findViewById(R.id.drawer_layout);
        t = new ActionBarDrawerToggle(this, dl,R.string.Open, R.string.Close);


        dl.addDrawerListener(t);
        t.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nv = (NavigationView)findViewById(R.id.nav_view);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.you_item:
                        //Toast.makeText(MainActivity.this, "Uno",Toast.LENGTH_SHORT).show();
                        showYouFragment();
                        break;
                        //poprawna reakcja
                    case R.id.your_series_item:
                        //Toast.makeText(MainActivity.this, "Dos",Toast.LENGTH_SHORT).show();
                        showSeriesFragment();
                        break;
                        //poprawna reakcja
                    default:
                        return true;
                }
                return true;
            }
        });
    }

    void showFragment(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentHolder, curr);
        fragmentTransaction.commit();
    }

    void showYouFragment()
    {
        curr = YouFragment.newInstance(myId);
        showFragment(curr);
    }
    void showSeriesFragment()
    {
        curr = SeriesFragment.newInstance(myId);
        showFragment(curr);
    }

    void showLogoutFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragmentHolder, LogoutFragment.newInstance());
        fragmentTransaction.commit();
    }

    void showLFriendsFragment()
    {
        curr = FriendsFragment.newInstance(myId);
        showFragment(curr);
    }

    @Override
    public void UsersChanged(List<User> users)
    {

    }
    @Override
    public void UserSelected(User user) {

        //TextView tv = (TextView) findViewById(R.id.text_view1);
        //tv.setText("Users: " + user._username);
    }

    @Override
    public void UserAdded() {

    }

    @Override
    public void SeriesChanged(List<Series> series)
    {

    }
    @Override
    public void SeriesSelected(Series series) {
        //TextView tv = (TextView) findViewById(R.id.text_view1);
        //tv.setText(series._title);
    }

    @Override
    public void AllRetrieved(List<User> u, List<Series> s, List<Rating> r, List<Friendship> f)
    {
        int i = 0;
        i += 1;
    }

    @Override
    public void RatingsChanged(List<Rating> ratings) {
        int i = 0;
    }

    @Override
    public void RatingSelected(Rating rating) {

    }

    @Override
    public void RatingChangeCompleted() {

    }
}
