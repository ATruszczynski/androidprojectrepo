package com.example.androidproject.Fragments;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.androidproject.FirebaseUtility.Rating;
import com.example.androidproject.FirebaseUtility.Series;
import com.example.androidproject.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeriesEditDialog extends DialogFragment {
    public int exitState = -1;
    boolean adding;
    public static Rating _rating;
    public static Series _series;
    public RatingEditInterface sli;
    public int orgRating;
    int newRating = 0;

    public SeriesEditDialog()
    {
    }
    public static SeriesEditDialog getInstance(Rating rating, Series series)
    {
        SeriesEditDialog sed = new SeriesEditDialog();
        _rating = rating;
        _series = series;
        sed.orgRating = _rating._rating;
        if(rating._rating == -1)
            sed.adding = true;
        else
            sed.adding = false;
        return  sed;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //LayoutInflater inflater = requireActivity().getLayoutInflater();


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout



        View result = inflater.inflate(R.layout.fragment_series_edit_dialog, container);

        TextView titleTV = (TextView)result.findViewById(R.id.seriesTitle);
        String title = _series._polishTitle;
        if(_series._polishTitle != _series._englishTitle)
            title += " (" + _series._englishTitle + ")";
        titleTV.setText(title);

        TextView yearTV = (TextView)result.findViewById(R.id.yearValue);
        yearTV.setText(Integer.toString(_series._yearReleased));

        TextView userScoreTV = (TextView)result.findViewById(R.id.userScoreValue);
        String rating = "";
        double ratingVal = _series._ratingValue;
        if(ratingVal < 0)
            rating += "- ";
        else
            rating += Double.toString(_series._ratingValue);
        rating += " (" + getString(R.string.from_text) + ": " + Integer.toString(_series._ratingCount) + " " + getString(R.string.votes) + ")";
        userScoreTV.setText(rating);



        TextView lengthTV = (TextView)result.findViewById(R.id.epLenValue);
        lengthTV.setText(Integer.toString(_series._length) + " min.");

        TextView countryTV = (TextView)result.findViewById(R.id.countryValue);
        String countries = ListToString(_series._countries);
        countryTV.setText(countries);

        TextView descTV = (TextView)result.findViewById(R.id.descriptionValue);
        descTV.setText(_series._description);

        TextView castTV = (TextView)result.findViewById(R.id.castValue);
        String cast = ListToString(_series._cast);
        castTV.setText(cast);

        TextView directorsTV = (TextView)result.findViewById(R.id.directorsValue);
        String dir = ListToString(_series._directors);
        directorsTV.setText(dir);

        if(_rating._rating != -1)
        {
            EditText ratingET = (EditText) result.findViewById(R.id.ratingEditor);
            ratingET.setText(Integer.toString(_rating._rating));
        }

        return result;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        EditText et = (EditText) SeriesEditDialog.this.getDialog().findViewById(R.id.ratingEditor);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                Button edit = (Button)SeriesEditDialog.this.getDialog().findViewById(R.id.acceptButton);
                //Button edit2 = getActivity().findViewById(R.id.acceptButton);
                try
                {
                    newRating = Integer.parseInt(s.toString());
                    if(newRating > 10 || newRating < 1)
                        throw new NumberFormatException();
                    //_rating._rating = newRating;
                    edit.setEnabled(true);
                }
                catch (NumberFormatException e)
                {
                    edit.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*builder.setView(inflater.inflate(R.layout.fragment_series_edit_dialog, null))
                .setMessage("OmegaDesu")
                .setPositiveButton(adding?"Add":"Edit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        exitState = 0;
                        if(adding)
                            sli.RatingAdded(_rating);
                        else
                            sli.RatingChanged(_rating);
                        SeriesEditDialog.this.getDialog().dismiss();

                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitState = 1;
                        _rating._rating = -1;
                        sli.RatingDeleted(_rating);
                        SeriesEditDialog.this.getDialog().dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        exitState = 2;
                        SeriesEditDialog.this.getDialog().cancel();
                    }
                });
        // Create the AlertDialog object and return it
        //return builder.create();*/

        Button accept = (Button) SeriesEditDialog.this.getDialog().findViewById(R.id.acceptButton);
        Button delete = (Button) SeriesEditDialog.this.getDialog().findViewById(R.id.deleteButton);
        Button cancel = (Button) SeriesEditDialog.this.getDialog().findViewById(R.id.cancelButton);

        if(adding)
        {
            accept.setText("Add");
            delete.setVisibility(View.INVISIBLE);
        }
        else
        {
            accept.setText("Edit");
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitState = 0;
                _rating._rating = newRating;
                if(adding)
                    sli.RatingAdded(_rating);
                else
                    sli.RatingChanged(_rating);
                SeriesEditDialog.this.getDialog().dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitState = 1;
                _rating._rating = -1;
                sli.RatingDeleted(_rating);
                SeriesEditDialog.this.getDialog().dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitState = 2;
                //_rating._rating = orgRating;
                SeriesEditDialog.this.getDialog().cancel();
            }
        });



        int dialogWidth = (int)(getActivity().getWindow().getDecorView().getWidth() * 0.9);
        int dialogHeight = (int)(getActivity().getWindow().getDecorView().getHeight() * 0.9);

        //dialogHeight = SeriesEditDialog.this.getDialog().getWindow().getDecorView().getHeight();
        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
    }

    String ListToString(List<String> list)
    {
        String result = "";
        if(list.size() > 0)
            result = list.get(0);
        for(int i = 1; i < list.size(); i++)
            result += ", " + list.get(i);
        return result;
    }

    public interface RatingEditInterface
    {
        public void RatingAdded(Rating rating);
        public void RatingChanged(Rating rating);
        public void RatingDeleted(Rating rating);
    }
}
