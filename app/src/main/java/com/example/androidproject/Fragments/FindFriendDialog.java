package com.example.androidproject.Fragments;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.androidproject.FirebaseUtility.FriendTuple;
import com.example.androidproject.FirebaseUtility.Rating;
import com.example.androidproject.FirebaseUtility.Series;
import com.example.androidproject.GeneralUtility.FriendsListAdapter;
import com.example.androidproject.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindFriendDialog extends DialogFragment {
    public List<FriendTuple> fuples;
    FriendsListAdapter fla;
    public FFInterface ffi;

    public FindFriendDialog()
    {
    }
    public static FindFriendDialog getInstance(List<FriendTuple> ftuples)
    {
        FindFriendDialog ffd = new FindFriendDialog();

        ffd.fuples = ftuples;

        return  ffd;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_find_friend_dialog, container);

        ListView lv = (ListView) getActivity().findViewById(R.id.friends_list);
        fla = new FriendsListAdapter(getActivity(), R.layout.friends_list_item, fuples);
        lv.setAdapter(fla);

        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.list_header_2,lv,false);

        TextView tmp = (TextView)header.findViewById(R.id.list_header_1);
        tmp.setText("Candidate:");
        tmp = (TextView)header.findViewById(R.id.list_header_2);
        tmp.setText("Score");

        if (lv.getHeaderViewsCount() < 1)
            lv.addHeaderView(header);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fuples.get(position - 1).friedly = !fuples.get(position).friedly;

            }
        });

        Button done = (Button) getActivity().findViewById(R.id.done_button);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ffi.FFDone(fuples);
                FindFriendDialog.this.getDialog().dismiss();
            }
        });

        return result;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        /*EditText et = (EditText) SeriesEditDialog.this.getDialog().findViewById(R.id.ratingEditor);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                Button edit = (Button)SeriesEditDialog.this.getDialog().findViewById(R.id.acceptButton);
                //Button edit2 = getActivity().findViewById(R.id.acceptButton);
                try
                {
                    newRating = Integer.parseInt(s.toString());
                    if(newRating > 10 || newRating < 1)
                        throw new NumberFormatException();
                    //_rating._rating = newRating;
                    edit.setEnabled(true);
                }
                catch (NumberFormatException e)
                {
                    edit.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*builder.setView(inflater.inflate(R.layout.fragment_series_edit_dialog, null))
                .setMessage("OmegaDesu")
                .setPositiveButton(adding?"Add":"Edit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        exitState = 0;
                        if(adding)
                            sli.RatingAdded(_rating);
                        else
                            sli.RatingChanged(_rating);
                        SeriesEditDialog.this.getDialog().dismiss();

                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitState = 1;
                        _rating._rating = -1;
                        sli.RatingDeleted(_rating);
                        SeriesEditDialog.this.getDialog().dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        exitState = 2;
                        SeriesEditDialog.this.getDialog().cancel();
                    }
                });
        // Create the AlertDialog object and return it
        //return builder.create();*/

        /*Button accept = (Button) SeriesEditDialog.this.getDialog().findViewById(R.id.acceptButton);
        Button delete = (Button) SeriesEditDialog.this.getDialog().findViewById(R.id.deleteButton);
        Button cancel = (Button) SeriesEditDialog.this.getDialog().findViewById(R.id.cancelButton);

        if(adding)
        {
            accept.setText("Add");
            delete.setVisibility(View.INVISIBLE);
        }
        else
        {
            accept.setText("Edit");
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitState = 0;
                _rating._rating = newRating;
                if(adding)
                    sli.RatingAdded(_rating);
                else
                    sli.RatingChanged(_rating);
                SeriesEditDialog.this.getDialog().dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitState = 1;
                _rating._rating = -1;
                sli.RatingDeleted(_rating);
                SeriesEditDialog.this.getDialog().dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitState = 2;
                //_rating._rating = orgRating;
                SeriesEditDialog.this.getDialog().cancel();
            }
        });*/



        int dialogWidth = (int)(getActivity().getWindow().getDecorView().getWidth() * 0.9);
        int dialogHeight = (int)(getActivity().getWindow().getDecorView().getHeight() * 0.9);

        //dialogHeight = SeriesEditDialog.this.getDialog().getWindow().getDecorView().getHeight();
        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
    }

    String ListToString(List<String> list)
    {
        String result = "";
        if(list.size() > 0)
            result = list.get(0);
        for(int i = 1; i < list.size(); i++)
            result += ", " + list.get(i);
        return result;
    }

    public interface RatingEditInterface
    {
        public void RatingAdded(Rating rating);
        public void RatingChanged(Rating rating);
        public void RatingDeleted(Rating rating);
    }

    public interface FFInterface
    {
        public void FFDone(List<FriendTuple> ftuples);
    }
}
