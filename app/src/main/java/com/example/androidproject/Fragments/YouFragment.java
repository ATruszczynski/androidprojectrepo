package com.example.androidproject.Fragments;

import com.google.firebase.auth.FirebaseAuth;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidproject.FirebaseUtility.FirestoreDB;
import com.example.androidproject.FirebaseUtility.Friendship;
import com.example.androidproject.FirebaseUtility.Rating;
import com.example.androidproject.FirebaseUtility.Series;
import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.FirebaseUtility.User;
import com.example.androidproject.GeneralUtility.FavListAdapter;
import com.example.androidproject.MainActivity;
import com.example.androidproject.R;

import java.util.LinkedList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link YouFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link YouFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class YouFragment extends Fragment implements FirestoreDB.UserListInterface, FirestoreDB.CompleteInterface
{

    private OnFragmentInteractionListener mListener;
    private String YourId;
    TupleAT[] currList = new TupleAT[0];
    FavListAdapter fla;
    FirestoreDB db;

    public YouFragment() {
        // Required empty public constructor
    }
    public static YouFragment newInstance(String yourId) {
        YouFragment fragment = new YouFragment();
        fragment.YourId = yourId;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_you, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        //FirebaseAuth.getInstance().getCurrentUser().getEmail();
        db = new FirestoreDB();
        db.addUserListener(this);
        db.addCompleteListener(this);
        db.getUser(YourId);

        ListView lv = (ListView) getActivity().findViewById(R.id.fav_list);
        fla = new FavListAdapter(getActivity(), R.layout.fav_list_item, currList);
        lv.setAdapter(fla);

        ViewGroup header = (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.list_header_2,lv,false);

        TextView tmp = (TextView)header.findViewById(R.id.list_header_1);
        tmp.setText("Series");
        tmp = (TextView)header.findViewById(R.id.list_header_2);
        tmp.setText("Your score");

        if (lv.getHeaderViewsCount() < 1)
            lv.addHeaderView(header);

        db.getAll();

        ListView favs = (ListView)getActivity().findViewById(R.id.fav_list);
        TextView favlab = (TextView)getActivity().findViewById(R.id.seriesLabel);

        favs.setVisibility(View.INVISIBLE);
        favlab.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onStop()
    {
        super.onStop();
        db.removeListener(this);
    }

    @Override
    public void UsersChanged(List<User> users) {

    }

    @Override
    public void UserSelected(User user)
    {
        TextView tv = (TextView) getActivity().findViewById(R.id.you_username);
        tv.setText(user._username);

        tv = (TextView) getActivity().findViewById(R.id.currUserName);
        tv.setText(user._username + " (" + FirebaseAuth.getInstance().getCurrentUser().getEmail() + ")");

        MainActivity.currUser = user;

        tv = (TextView) getActivity().findViewById(R.id.you_age);
        tv.setText(String.valueOf(user._age));

        tv = (TextView) getActivity().findViewById(R.id.you_sex);
        tv.setText(user._sex);


    }

    @Override
    public void UserAdded() {

    }

    @Override
    public void AllRetrieved(List<User> u, List<Series> s, List<Rating> r, List<Friendship> f)
    {
        User you = null;
        for(User uu: u)
        {
            if(uu._id == YourId)
            {
                you = uu;
                break;
            }
        }
        List<Rating> usersRatings = new LinkedList<>();
        for(Rating rr: r)
        {
            if(rr._userId.equals(YourId))
                usersRatings.add(rr);
        }
        TupleAT[] list = new TupleAT[usersRatings.size()];
        int i = 0;

        for(Rating rr: usersRatings)
        {
            for(Series ss: s)
            {
                if(ss._id == rr._seriesId)
                {
                    list[i++] = new TupleAT(you, ss,rr);
                    break;
                }
            }
        }

        if(s.size() == 0)
            list = new TupleAT[0];

        ListView lv = (ListView) getActivity().findViewById(R.id.fav_list);
        fla = new FavListAdapter(getActivity(), R.layout.fav_list_item, list);
        lv.setAdapter(fla);

        if(list.length > 0)
        {
            ListView favs = (ListView)getActivity().findViewById(R.id.fav_list);
            TextView favlab = (TextView)getActivity().findViewById(R.id.seriesLabel);

            favs.setVisibility(View.VISIBLE);
            favlab.setVisibility(View.VISIBLE);
        }

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
