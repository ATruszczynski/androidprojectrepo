package com.example.androidproject.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidproject.FirebaseUtility.FirestoreDB;
import com.example.androidproject.FirebaseUtility.FriendTuple;
import com.example.androidproject.FirebaseUtility.Friendship;
import com.example.androidproject.FirebaseUtility.Rating;
import com.example.androidproject.FirebaseUtility.Series;
import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.FirebaseUtility.User;
import com.example.androidproject.GeneralUtility.FriendsListAdapter;
import com.example.androidproject.GeneralUtility.SeriesListAdapter;
import com.example.androidproject.MainActivity;
import com.example.androidproject.R;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class FriendsFragment extends Fragment implements FirestoreDB.CompleteInterface, FirestoreDB.RatingsInterface, FirestoreDB.FriendshipsInteface
{
    private String YourId;
    List<Integer> seriesIds;
    List<FriendTuple> friends;
    FriendsListAdapter fla;
    FirestoreDB db;

    public FriendsFragment() {
        // Required empty public constructor
    }

    public static FriendsFragment newInstance(String yourId) {
        FriendsFragment fragment = new FriendsFragment();
        fragment.YourId = yourId;
        fragment.seriesIds = new LinkedList<>();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friends, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        if(MainActivity.currUser != null)
        {
            TextView tv = (TextView) getActivity().findViewById(R.id.currUserName);
            tv.setText(MainActivity.currUser._username);
        }

        //seriesIds = new LinkedList<>();
        friends = new LinkedList<>();

        db = new FirestoreDB();
        db.addCompleteListener(this);
        db.addFriendshipListener(this);
        //db.addRatingListener(this);
        //db.addUserListener(this);
        //db.getUser(YourId);

        ListView lv = (ListView) getActivity().findViewById(R.id.friends_list);
        fla = new FriendsListAdapter(getActivity(), R.layout.friends_list_item, friends);
        lv.setAdapter(fla);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.list_header_2,lv,false);

        TextView tmp = (TextView)header.findViewById(R.id.list_header_1);
        tmp.setText("Friend");
        tmp = (TextView)header.findViewById(R.id.list_header_2);
        tmp.setText("Score");

        if (lv.getHeaderViewsCount() < 1)
            lv.addHeaderView(header);

        db.getAll();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        db.removeListener(this);
    }

    @Override
    public void AllRetrieved(List<User> u, List<Series> s, List<Rating> r, List<Friendship> f)
    {
        friends.clear();

        List<Friendship> myFriends = new LinkedList<>();
        for(Friendship friendship : f)
        {
            if(friendship._user1.compareTo(YourId) == 0)
            {
                myFriends.add(friendship);
            }
            else if(friendship._user2.compareTo(YourId) == 0)
            {
                myFriends.add(new Friendship(friendship._id, friendship._user2, friendship._user1));
            }
        }

        Map<String, List<Rating>> ratingDict = new HashMap<String, List<Rating>>();
        ratingDict.put(YourId, new LinkedList<Rating>());


        for(Rating rating: r)
        {
            if(!ratingDict.containsKey(rating._userId))
            {
                ratingDict.put(rating._userId, new LinkedList<Rating>());
            }
            ratingDict.get(rating._userId).add(rating);
        }

        List<Rating> yourRatings = ratingDict.get(YourId);
        ratingDict.remove(YourId);

        List<FriendTuple> scores = new LinkedList<>();

        for(String userId: ratingDict.keySet())
        {
            int metric = 0;
            List<Rating> userRats = ratingDict.get(userId);
            for(Rating userRat : userRats)
            {
                for(Rating yourRat: yourRatings)
                {
                    if(userRat._seriesId == yourRat._seriesId)
                        metric += metricF(userRat, yourRat);

                }
            }
            scores.add(new FriendTuple(userId, metric));
        }

        for(FriendTuple ft : scores)
        {
            for(Friendship mf: myFriends)
            {
                if(ft.friendCand.compareTo(mf._user2) == 0) {
                    friends.add(ft);
                }
            }
        }
        //dodaj coś

        fla.notifyDataSetChanged();
    }

    int metricF(Rating r1, Rating r2)
    {
        return 10 - (StrictMath.abs(r1._rating - r2._rating));
    }

    @Override
    public void RatingsChanged(List<Rating> ratings) {

    }

    @Override
    public void RatingSelected(Rating rating) {

    }

    @Override
    public void RatingChangeCompleted()
    {
        fla.notifyDataSetChanged();
    }

    @Override
    public void FriendshipsChanged() {

    }

    @Override
    public void FriendshipGotAll(List<Friendship> friendships) {
    }
}
