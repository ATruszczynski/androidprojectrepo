package com.example.androidproject.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.androidproject.FirebaseUtility.FirestoreDB;
import com.example.androidproject.FirebaseUtility.Friendship;
import com.example.androidproject.FirebaseUtility.Rating;
import com.example.androidproject.FirebaseUtility.Series;
import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.FirebaseUtility.User;
import com.example.androidproject.GeneralUtility.SeriesListAdapter;
import com.example.androidproject.MainActivity;
import com.example.androidproject.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class SeriesFragment extends Fragment implements FirestoreDB.CompleteInterface, FirestoreDB.RatingsInterface, SeriesEditDialog.RatingEditInterface
{
    private String YourId;
    List<Integer> seriesIds;
    List<TupleAT> tatss;
    SeriesListAdapter fla;
    FirestoreDB db;

    public SeriesFragment() {
        // Required empty public constructor
    }

    public static SeriesFragment newInstance(String yourId) {
        SeriesFragment fragment = new SeriesFragment();
        fragment.db = new FirestoreDB();
        fragment.YourId = yourId;
        fragment.seriesIds = new LinkedList<>();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_series, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        if(MainActivity.currUser != null)
        {
            TextView tv = (TextView) getActivity().findViewById(R.id.currUserName);
            tv.setText(MainActivity.currUser._username);
        }

        seriesIds = new LinkedList<>();
        tatss = new LinkedList<>();

        //final FirestoreDB db = new FirestoreDB();
        db.addCompleteListener(this);
        db.addRatingListener(this);
        //db.addUserListener(this);
        //db.getUser(YourId);

        ListView lv = (ListView) getActivity().findViewById(R.id.series_list);
        fla = new SeriesListAdapter(getActivity(), R.layout.series_list_item, tatss);
        lv.setAdapter(fla);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.list_header_2,lv,false);

        TextView tmp = (TextView)header.findViewById(R.id.list_header_1);
        tmp.setText(R.string.seriesHeader);
        tmp = (TextView)header.findViewById(R.id.list_header_2);
        tmp.setText("");

        if (lv.getHeaderViewsCount() < 1)
            lv.addHeaderView(header);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*if(tatss.get(position)._rating._rating == -1)
                {
                    Toast.makeText(getActivity(), "Not yet", Toast.LENGTH_SHORT).show();
                    tatss.get(position)._rating._rating = 5;
                    //db.addRating(tatss.get(position)._rating);

                }
                else {
                    Toast.makeText(getActivity(), "Already there", Toast.LENGTH_SHORT).show();
                    tatss.get(position)._rating._rating = -1;
                    //db.deleteRating(tatss.get(position)._rating);
                }*/
                SeriesEditDialog sed = SeriesEditDialog.getInstance(tatss.get(position-1)._rating, tatss.get(position - 1)._series);
                sed.show(getFragmentManager(), "desu");
                sed.sli = SeriesFragment.this;
            }
        });


        db.getAll();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void AllRetrieved(List<User> u, List<Series> s, List<Rating> r, List<Friendship> f)
    {
        User you = null;
        int i = 0;
        for(User uu: u)
        {
            if(uu._id.compareTo(YourId) == 0)//uu._id == YourId)
            {
                you = uu;
                break;
            }
        }

        TextView tv = (TextView) getActivity().findViewById(R.id.currUserName);
        tv.setText(you._username + " (" + FirebaseAuth.getInstance().getCurrentUser().getEmail() + ")");
        List<TupleAT> tats = new LinkedList<>();
        for(Series ss: s)
        {
            Rating usrr = new Rating(you._id,ss._id, -1);
            for(Rating rr: r)
            {
                if(rr._userId.compareTo(you._id) == 0 && rr._seriesId == ss._id)
                {
                    usrr = rr;
                    break;
                }
            }
            tats.add(new TupleAT(you, ss, usrr));
        }
        tatss.clear();
        for(TupleAT tat: tats)
        {
            tatss.add(tat);
        }

        tatss.sort(new Comparator<TupleAT>() {
            @Override
            public int compare(TupleAT o1, TupleAT o2) {
                if(o1._series._ratingCount == -1 && o2._series._ratingCount != -1)
                    return 1;
                if(o2._series._ratingCount == -1 && o1._series._ratingCount != -1)
                    return -1;
                if(o1._series._ratingCount>50000 && o2._series._ratingCount<50000)
                    return -1;
                if(o2._series._ratingCount>50000 && o1._series._ratingCount<50000)
                    return 1;
                return o2._rating._rating - o1._rating._rating;
            }
        });

        fla.notifyDataSetChanged();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        db.removeListener(this);
    }

    @Override
    public void RatingsChanged(List<Rating> ratings) {

    }

    @Override
    public void RatingSelected(Rating rating) {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void RatingChangeCompleted()
    {
        //tatss.add(new TupleAT());
        ///tatss.remove(tatss.size() - 1);
        tatss.sort(new Comparator<TupleAT>() {
            @Override
            public int compare(TupleAT o1, TupleAT o2) {
                return o2._rating._rating - o1._rating._rating;
            }
        });
        fla.notifyDataSetChanged();
    }

    @Override
    public void RatingChanged(Rating rating) {
        db.EditRating(rating);
    }

    @Override
    public void RatingDeleted(Rating rating) {
        db.deleteRating(rating);
    }

    @Override
    public void RatingAdded(Rating rating)
    {
        db.addRating(rating);
    }
}
