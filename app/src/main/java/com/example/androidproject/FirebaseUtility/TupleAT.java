package com.example.androidproject.FirebaseUtility;

public class TupleAT
{
    public User _user;
    public Series _series;
    public Rating _rating;

    public TupleAT()
    {

    }


    public TupleAT(User user, Series series, Rating rating)
    {
        _user = user;
        _series = series;
        _rating = rating;
    }
}
