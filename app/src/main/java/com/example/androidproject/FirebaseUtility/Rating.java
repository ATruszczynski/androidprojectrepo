package com.example.androidproject.FirebaseUtility;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.HashMap;

public class Rating
{
    static String userIdKey = "userId";
    static String seriesIdKey = "seriesId";
    static String ratingKey = "rating";

    public String _userId;
    public int _seriesId;
    public int _rating;

    public Rating(String userId, int seriesId, int rating)
    {
        _userId = userId;
        _seriesId = seriesId;
        _rating = rating;
    }

    public HashMap<String, Object> ToHashMap()
    {
        HashMap<String, Object> result = new HashMap<>();

        result.put(userIdKey, _userId);
        result.put(seriesIdKey, _seriesId);
        result.put(ratingKey, _rating);

        return result;
    }

    public static Rating FromRatingDC(DocumentSnapshot dc)
    {
        String userId = (String)dc.get(userIdKey);
        int seriesId = ((Long)dc.get(seriesIdKey)).intValue();
        int rating = ((Long)dc.get(ratingKey)).intValue();

        return new Rating(userId,seriesId,rating);
    }
}
