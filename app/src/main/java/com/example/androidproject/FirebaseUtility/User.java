package com.example.androidproject.FirebaseUtility;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class User
{
    static String idKey = "id";
    static String usernameKey = "username";
    static String sexKey = "sex";
    static String ageKey = "age";
    static String seriesKey = "series";

    public String _id;
    public String _username;
    public String _sex;
    public int _age;
    public List<Integer> _series;

    public User(String id, String username, String sex, int age)
    {
        _id = id;
        _username = username;
        _sex = sex;
        _age = age;
        _series = new LinkedList<>();
    }
    public User(String id, String username, String sex, int age, List<Integer> series)
    {
        _id = id;
        _username = username;
        _sex = sex;
        _age = age;
        _series = series;
    }

    public HashMap<String, Object> ToHashMap()
    {
        HashMap<String, Object> result = new HashMap<>();

        result.put(idKey, _id);
        result.put(usernameKey, _username);
        result.put(sexKey, _sex);
        result.put(ageKey, _age);
        result.put(seriesKey, _series);

        return result;
    }

    public static User FromUserDC (DocumentSnapshot dc)
    {
        String id = (String)dc.get(idKey);
        String username = (String)dc.get(usernameKey);
        String sex = (String)dc.get(sexKey);
        int age = ((Long)dc.get(ageKey)).intValue();
        List<Integer> series = (List<Integer>)dc.get(seriesKey);

        return new User(id,username,sex,age,series);
    }
}
