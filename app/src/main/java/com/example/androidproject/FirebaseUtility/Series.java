package com.example.androidproject.FirebaseUtility;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Series
{
    static String idKey = "id";
    static String polishTitleKey = "ptitle";
    static String englishTitleKey = "etitle";
    static String descriptionKey = "desc";
    static String ratingValueKey = "rval";
    static String ratingCountKey = "rcnt";
    static String yearReleasedKey = "yearReleased";
    static String lengthKey = "len";
    static String genreKey = "genre";
    static String countriesKey = "countries";
    static String directorsKey = "directors";
    static String castKey = "cast";

    public int _id;
    public String _polishTitle;
    public String _englishTitle;
    public String _description;
    public double _ratingValue;
    public int _ratingCount;
    public int _yearReleased;
    public int _length;
    public String _genre;
    public List<String> _countries;
    public List<String> _directors;
    public List<String> _cast;

    public Series(String pTitle, String eTitle, String desc, double ratingV, int ratingC, int yearReleased, int len, String genre, List<String> countries, List<String> directors, List<String> cast)
    {
        _polishTitle = pTitle;
        _englishTitle = eTitle;
        _description = desc;
        _ratingValue = ratingV;
        _ratingCount = ratingC;
        _yearReleased = yearReleased;
        _length = len;
        _genre = genre;
        _countries = countries;
        _directors = directors;
        _cast = cast;
    }
    public Series()
    {

    }

    public Series(String title, int yearReleased)
    {
        _polishTitle = title;
        _yearReleased = yearReleased;
    }

    public HashMap<String, Object> ToHashMap()
    {
        HashMap<String, Object> result = new HashMap<>();

        result.put(idKey, _id);
        result.put(polishTitleKey, _polishTitle);
        result.put(englishTitleKey, _englishTitle);
        result.put(descriptionKey, _description);
        result.put(ratingValueKey, _ratingValue);
        result.put(ratingCountKey, _ratingCount);
        result.put(yearReleasedKey, _yearReleased);
        result.put(lengthKey, _length);
        result.put(genreKey, _genre);
        result.put(countriesKey, _countries);
        result.put(directorsKey, _directors);
        result.put(castKey, _cast);

        return result;
    }

    public static Series FromSeriesDC(DocumentSnapshot dc)
    {
        Series result = new Series();

        result._id = ((Long)dc.get(idKey)).intValue();
        result._polishTitle = (String)dc.get(polishTitleKey);
        result._englishTitle = (String)dc.get(englishTitleKey);
        result._description = (String)dc.get(descriptionKey);
        double ratVal = (double)dc.get(ratingValueKey);

        result._ratingValue = Math.floor(ratVal*100)/100;

        result._ratingCount = ((Long)dc.get(ratingCountKey)).intValue();
        result._yearReleased = ((Long)dc.get(yearReleasedKey)).intValue();
        result._length = ((Long)dc.get(lengthKey)).intValue();
        result._genre = (String)dc.get(genreKey);
        result._countries = (List<String>)dc.get(countriesKey);
        result._directors = (List<String>)dc.get(directorsKey);
        result._cast = (List<String>)dc.get(castKey);

        return result;
    }
}
