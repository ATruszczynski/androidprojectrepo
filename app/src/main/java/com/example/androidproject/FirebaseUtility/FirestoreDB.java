package com.example.androidproject.FirebaseUtility;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.Query;
import com.google.firebase.database.core.utilities.Pair;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nullable;

public class FirestoreDB
{
    private static final String usersCollectionId = "users";
    private static final String seriesCollectionId = "series";
    private static final String ratingsCollectionId = "ratings";
    private static final String friendshipsCollectionId = "friendships";
    private static final String stats = "stats";
    private static final String seriesData = "seriesData";
    private static final String idKey = "id";
    List<UserListInterface> userListInterfaces = new LinkedList<>();
    List<SeriesListInterface> seriesListInterfaces = new LinkedList<>();
    List<RatingsInterface> ratingsListInterfaces = new LinkedList<>();
    List<FriendshipsInteface> friendshipsIntefacesInterfaces = new LinkedList<>();
    List<CompleteInterface> completeInterfaces = new LinkedList<>();

    List<User> jUsrs = new LinkedList<>();
    List<Series> jSrs = new LinkedList<>();
    List<Rating> jRts = new LinkedList<>();
    List<Friendship> jFrens = new LinkedList<>();

    FirebaseFirestore db;

    public FirestoreDB()
    {

        userListInterfaces = new LinkedList<>();
        seriesListInterfaces = new LinkedList<>();
        ratingsListInterfaces = new LinkedList<>();
        db = FirebaseFirestore.getInstance();

    }


    //region Add methods

    public void addUser(User user)
    {
        db.collection(usersCollectionId).add(user.ToHashMap()).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                for(UserListInterface fi: userListInterfaces)
                    fi.UserAdded();
            }
        });

    }

    public void addSeries(final List<Series> series)
    {

        //db.collection(seriesCollectionId).add(series.ToHashMap());
        //firebase.firestore.
        db.collection(stats).document(seriesData).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Long idl = ((Long)documentSnapshot.get(idKey));
                int id = idl.intValue();

                for (Series s: series)
                {
                    s._id = id;
                    db.collection(seriesCollectionId).add(s.ToHashMap());
                    id++;
                }

                //series._id = id;
                //db.collection(seriesCollectionId).add(series.ToHashMap());
                //id++;
                HashMap<String, Object> data = new HashMap<>();
                data.put(idKey, id);
                db.collection(stats).document(seriesData).update(data);
            }
        });



    }

    public void addRating(Rating usr)
    {
        db.collection(ratingsCollectionId).add(usr.ToHashMap()).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                for(RatingsInterface ri: ratingsListInterfaces)
                    ri.RatingChangeCompleted();
            }
        });

    }
    public void addFriendship(Friendship friendship)
    {
        db.collection(friendshipsCollectionId).add(friendship.ToHashMap()).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                for(FriendshipsInteface fi: friendshipsIntefacesInterfaces)
                    fi.FriendshipsChanged();
            }
        });
    }

    //endregion

    //region Add listeners

    public void addUserListener(UserListInterface ucl)
    {
        userListInterfaces.add(ucl);

        db.collection(usersCollectionId).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (UserListInterface ucl: userListInterfaces)
                {
                    List<DocumentSnapshot> d = queryDocumentSnapshots.getDocuments();
                    List<User> u = new LinkedList<>();
                    for(DocumentSnapshot dc: d)
                    {
                        u.add(User.FromUserDC(dc));
                    }
                    ucl.UsersChanged(u);
                }
            }
        });
    }
    public void addSeriesListener(SeriesListInterface ucl)
    {
        seriesListInterfaces.add(ucl);

        db.collection(seriesCollectionId).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (SeriesListInterface sli: seriesListInterfaces)
                {
                    List<DocumentSnapshot> d = queryDocumentSnapshots.getDocuments();
                    List<Series> u = new LinkedList<>();
                    for(DocumentSnapshot dc: d)
                    {
                        u.add(Series.FromSeriesDC(dc));
                    }
                    sli.SeriesChanged(u);
                }
            }
        });
    }
    public void addRatingListener(RatingsInterface usrl)
    {
        ratingsListInterfaces.add(usrl);

        db.collection(ratingsCollectionId).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (RatingsInterface usrl: ratingsListInterfaces)
                {
                    List<DocumentSnapshot> d = queryDocumentSnapshots.getDocuments();
                    List<Rating> u = new LinkedList<>();
                    for(DocumentSnapshot dc: d)
                    {
                        u.add(Rating.FromRatingDC(dc));
                    }
                    usrl.RatingsChanged(u);
                }
            }
        });
    }
    public void addFriendshipListener(FriendshipsInteface frls)
    {
        friendshipsIntefacesInterfaces.add(frls);
    }
    public void addCompleteListener(CompleteInterface ci)
    {
        completeInterfaces.add(ci);
    }

    //endregion

    //region RemoveList

    public void removeListener(Object listener)
    {
        userListInterfaces.remove(listener);
        ratingsListInterfaces.remove(listener);
        seriesListInterfaces.remove(listener);
        friendshipsIntefacesInterfaces.remove(listener);
        completeInterfaces.remove(listener);
    }

    //endregion

    //region Deletes
    public void deleteRating(Rating rating)
    {
        db.collection(ratingsCollectionId).whereEqualTo(Rating.userIdKey, rating._userId)
                .whereEqualTo(Rating.seriesIdKey, rating._seriesId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                DocumentReference dref = queryDocumentSnapshots.getDocuments().get(0).getReference();
                dref.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        for(RatingsInterface ri: ratingsListInterfaces)
                            ri.RatingChangeCompleted();
                    }
                });
            }
        });
    }
    public void deleteFriendship(Friendship friendship)
    {
        db.collection(friendshipsCollectionId).whereEqualTo(Friendship.idKey, friendship._id).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                DocumentReference dref = queryDocumentSnapshots.getDocuments().get(0).getReference();
                dref.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        for(FriendshipsInteface ri: friendshipsIntefacesInterfaces)
                            ri.FriendshipsChanged();
                    }
                });
            }
        });
    }
    //endregion

    //region Getters

    public void getUser(String userId)
    {
        db.collection(usersCollectionId).whereEqualTo(User.idKey, userId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<DocumentSnapshot> dd = queryDocumentSnapshots.getDocuments();
                DocumentSnapshot d = dd.get(0);
                for(UserListInterface uli: userListInterfaces)
                {
                    uli.UserSelected(User.FromUserDC(d));
                }
            }
        });
    }
    public void getSeries(int seriesId)
    {
        db.collection(seriesCollectionId).whereEqualTo(Series.idKey, seriesId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<DocumentSnapshot> dd = queryDocumentSnapshots.getDocuments();
                DocumentSnapshot d = dd.get(0);
                for(SeriesListInterface sli: seriesListInterfaces)
                {
                    sli.SeriesSelected(Series.FromSeriesDC(d));
                }
            }
        });
    }
    public void getUsersRatings(int userId)
    {
        
        db.collection(ratingsCollectionId).whereEqualTo(Rating.userIdKey, userId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<DocumentSnapshot> dd = queryDocumentSnapshots.getDocuments();
                List<Rating> results = new LinkedList<>();
                for(DocumentSnapshot ds: dd)
                {
                    results.add(Rating.FromRatingDC(ds));
                }
                for(RatingsInterface ri: ratingsListInterfaces)
                {
                    ri.RatingsChanged(results);
                }
            }
        });
    }

    public void getFriends()
    {
        db.collection(friendshipsCollectionId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<Friendship> friends = new LinkedList<>();
                List<DocumentSnapshot> dd = queryDocumentSnapshots.getDocuments();
                for(DocumentSnapshot d: dd)
                {
                    friends.add(Friendship.FromFriendshipDC(d));
                }
                for(FriendshipsInteface fri: friendshipsIntefacesInterfaces)
                {
                    fri.FriendshipGotAll(friends);
                }
            }
        });
    }

    public void getAll()
    {
        jUsrs = new LinkedList<>();
        jSrs = new LinkedList<>();
        jRts = new LinkedList<>();

        db.collection(usersCollectionId).get().continueWithTask(new Continuation<QuerySnapshot, Task<QuerySnapshot>>()
        {
            @Override
            public Task<QuerySnapshot> then(@NonNull Task<QuerySnapshot> task) throws Exception
            {
                List<DocumentSnapshot> ds = task.getResult().getDocuments();
                for(DocumentSnapshot d: ds)
                    jUsrs.add(User.FromUserDC(d));
                return db.collection(seriesCollectionId).get();
            }
        }).continueWithTask(new Continuation<QuerySnapshot, Task<QuerySnapshot>>() {
            @Override
            public Task<QuerySnapshot> then(@NonNull Task<QuerySnapshot> task) throws Exception
            {
                List<DocumentSnapshot> ds = task.getResult().getDocuments();
                for(DocumentSnapshot d: ds)
                    jSrs.add(Series.FromSeriesDC(d));
                return db.collection(ratingsCollectionId).get();
            }
        }).continueWithTask(new Continuation<QuerySnapshot, Task<QuerySnapshot>>() {
            @Override
            public Task<QuerySnapshot> then(@NonNull Task<QuerySnapshot> task) throws Exception
            {
                List<DocumentSnapshot> ds = task.getResult().getDocuments();
                for(DocumentSnapshot d: ds)
                    jRts.add(Rating.FromRatingDC(d));
                return db.collection(friendshipsCollectionId).get();
            }
        }).addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<DocumentSnapshot> ds = queryDocumentSnapshots.getDocuments();
                for(DocumentSnapshot d: ds)
                    jFrens.add(Friendship.FromFriendshipDC(d));
                for(CompleteInterface ci: completeInterfaces)
                    ci.AllRetrieved(jUsrs, jSrs, jRts, jFrens);
            }
        });
    }
    //endregion

    //region Edit
    public void EditRating(final Rating rating)
    {
        db.collection(ratingsCollectionId).whereEqualTo(Rating.userIdKey, rating._userId)
                .whereEqualTo(Rating.seriesIdKey, rating._seriesId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                DocumentReference dref = queryDocumentSnapshots.getDocuments().get(0).getReference();
                dref.update(Rating.ratingKey, rating._rating).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        for(RatingsInterface ri: ratingsListInterfaces)
                            ri.RatingChangeCompleted();
                    }
                });
            }
        });
    }
    //endregion

    //region Interfaces
    public interface UserListInterface
    {
        public void UsersChanged(List<User> users);
        public void UserSelected(User user);
        public void UserAdded();
    }
    public interface SeriesListInterface
    {
        public void SeriesChanged(List<Series> series);
        public void SeriesSelected(Series series);
    }
    public interface RatingsInterface
    {
        public void RatingsChanged(List<Rating> ratings);
        public void RatingSelected(Rating rating);
        public void RatingChangeCompleted();

    }
    public interface FriendshipsInteface
    {
        public void FriendshipsChanged();
        public void FriendshipGotAll(List<Friendship> friendships);
    }
    public interface CompleteInterface
    {
        public void AllRetrieved(List<User> u, List<Series> s, List<Rating> r, List<Friendship> f);
    }
    //endregion
}
