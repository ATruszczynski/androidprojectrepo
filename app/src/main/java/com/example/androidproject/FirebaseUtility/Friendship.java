package com.example.androidproject.FirebaseUtility;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Friendship
{
    static String idKey = "id";
    static String user1Key = "user1";
    static String user2Key = "user2";

    public String _id;
    public String _user1;
    public String _user2;

    public Friendship(String id, String user1, String user2)
    {
        _id = id;
        _user1 = user1;
        _user2 = user2;
    }

    public HashMap<String, Object> ToHashMap()
    {
        HashMap<String, Object> result = new HashMap<>();

        result.put(idKey, _id);
        result.put(user1Key, _user1);
        result.put(user2Key, _user2);

        return result;
    }

    public static Friendship FromFriendshipDC (DocumentSnapshot dc)
    {
        String id = (String)dc.get(idKey);
        String user1 = (String)dc.get(user1Key);
        String user2 = (String)dc.get(user2Key);

        return new Friendship(id, user1, user2);
    }
}
