package com.example.androidproject.GeneralUtility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.R;

import java.util.LinkedList;
import java.util.List;

public class SeriesListAdapter extends ArrayAdapter<TupleAT>
{
    List<TupleAT> items;
    Context _context;
    public SeriesListAdapter(Context context, int resource, List<TupleAT> objects)
    {
        super(context, resource, objects);
        items = objects;
        _context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TupleAT curr = items.get(position);
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.series_list_item, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.stitle);
        title.setText(curr._series._polishTitle);

        TextView score = (TextView) rowView.findViewById(R.id.ratingNote);
        String ratingVal = "";
        if(curr._series._ratingValue < 0)
            ratingVal += "-";
        else
            ratingVal += Double.toString(curr._series._ratingValue);
        score.setText(_context.getString(R.string.rating) + ": " +  ratingVal);


        TextView rat = (TextView) rowView.findViewById(R.id.srating);
        if(curr._rating._rating != -1)
        {
            rat.setText(String.valueOf(curr._rating._rating));
            if(curr._series._ratingValue <0)
                score.setText(_context.getString(R.string.rating) + ": " +  curr._rating._rating);
        }
        else
        {
            rat.setText("-");
        }

        TextView genre = (TextView) rowView.findViewById(R.id.genreNote);

        genre.setText(_context.getString(R.string.genre) + ": " + curr._series._genre);

        return rowView;
    }
}
