package com.example.androidproject.GeneralUtility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.R;

public class FavListAdapter extends ArrayAdapter<TupleAT>
{
    TupleAT[] items;
    Context _context;
    public FavListAdapter(Context context, int resource, TupleAT[] objects)
    {
        super(context, resource, objects);
        items = objects;
        _context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TupleAT curr = items[position];
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.fav_list_item, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.ftitle);
        title.setText(curr._series._polishTitle);
        TextView rat = (TextView) rowView.findViewById(R.id.frating);
        rat.setText(String.valueOf(curr._rating._rating));

        return rowView;
    }
}
