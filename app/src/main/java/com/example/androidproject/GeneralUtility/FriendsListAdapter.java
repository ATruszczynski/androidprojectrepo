package com.example.androidproject.GeneralUtility;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidproject.FirebaseUtility.FriendTuple;
import com.example.androidproject.FirebaseUtility.Friendship;
import com.example.androidproject.FirebaseUtility.TupleAT;
import com.example.androidproject.R;

import java.util.LinkedList;
import java.util.List;

public class FriendsListAdapter extends ArrayAdapter<FriendTuple>
{
    List<FriendTuple> items;
    Context _context;
    public FriendsListAdapter(Context context, int resource, List<FriendTuple> objects)
    {
        super(context, resource, objects);
        items = objects;
        _context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        FriendTuple curr = items.get(position);
        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.friends_list_item, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.fname);
        title.setText(curr.friendCand);
        TextView metric = (TextView) rowView.findViewById(R.id.fmetric);
        metric.setText(Integer.toString(curr.closenessScore));

        if(curr.friedly)
            rowView.setBackgroundColor(Color.parseColor("#FFCCDD"));

        return rowView;
    }
}
