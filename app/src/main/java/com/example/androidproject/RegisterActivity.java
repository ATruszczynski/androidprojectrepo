package com.example.androidproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.androidproject.FirebaseUtility.FirestoreDB;
import com.example.androidproject.FirebaseUtility.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class RegisterActivity extends AppCompatActivity implements FirestoreDB.UserListInterface
{

    EditText email, password, username, year;
    Spinner sex;
    Button registerBtn, loginBtn;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final FirestoreDB db = new FirestoreDB();
        db.addUserListener(this);

        email = findViewById(R.id.userEmail);
        password = findViewById(R.id.userPassword);
        username = findViewById(R.id.userNick);
        year = findViewById(R.id.userBirth);
        sex = findViewById(R.id.userSex);


        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.registerBtn);

        firebaseAuth = FirebaseAuth.getInstance();

        final Spinner spinner = (Spinner) findViewById(R.id.userSex);
// Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sex_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String e = email.getText().toString();
                    String p = password.getText().toString();
                    final String u = username.getText().toString();
                    final String s = spinner.getSelectedItem().toString();
                    final int a = 2019 - Integer.parseInt(year.getText().toString());

                    if(TextUtils.isEmpty(e)
                        || TextUtils.isEmpty(p)
                        || TextUtils.isEmpty(u)){
                        Toast.makeText(getApplicationContext(), "Wypełnij wymagane pola", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(p.length()<8){
                        Toast.makeText(getApplicationContext(), "Hasło musi posiadać co najmniej 8 znaków", Toast.LENGTH_SHORT).show();
                    }

                    firebaseAuth.createUserWithEmailAndPassword(e,p)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                        if(task.isSuccessful()){



                                            String id = firebaseAuth.getUid();

                                            db.addUser(new User(id, u, s, a));


                                        }
                                        else{
                                            Toast.makeText(getApplicationContext(), "E-mail lub hasło jest błędne", Toast.LENGTH_SHORT).show();
                                        }
                                }
                            });


            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        if(firebaseAuth.getCurrentUser()!=null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }



    @Override
    public void UsersChanged(List<User> users) {

    }

    @Override
    public void UserSelected(User user) {

    }

    @Override
    public void UserAdded() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
}
